<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Department::insert([
			[
				'name' => Department::DEVELOPMENT_DEPARTMENT
			],
			[
				'name' => Department::TEST_DEPARTMENT
			],
			[
				'name' => Department::SEO_DEPARTMENT
			]
		]);
	}
}
