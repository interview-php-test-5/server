<?php

use App\Models\Department;
use App\Models\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
	$positions = [
		Employee::JUNIOR_POSITION,
		Employee::MIDDLE_POSITION,
		Employee::SENIOR_POSITION
	];
	$type = rand(0, 1) ? Employee::MONTHLY_RATE_TYPE : Employee::HOURLY_RATE_TYPE;
	return [
		'full_name' => $faker->firstName . ' ' . $faker->lastName,
		'birth_date' => $faker->dateTimeThisCentury->format('Y-m-d'),
		'type' => $type,
		'department_id' => Department::all()->random()->id,
		'position' => $positions[array_rand($positions)],
		'number_of_hours' => $type == Employee::HOURLY_RATE_TYPE ? rand(140, 180) : null,
		'hourly_rate' => $type == Employee::HOURLY_RATE_TYPE ? rand(5, 10) : null
	];
});
