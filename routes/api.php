<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
	[
		'prefix' => 'employee'
	],
	function () {
		Route::get('', 'EmployeeController@index');
	}
);

Route::group(
	[
		'prefix' => 'department'
	],
	function () {
		Route::get('', 'DepartmentController@index');
	}
);
