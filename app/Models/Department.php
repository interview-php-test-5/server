<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	const DEVELOPMENT_DEPARTMENT = 'development';
	const TEST_DEPARTMENT = 'test';
	const SEO_DEPARTMENT = 'seo';

	protected $fillable = [
		'name'
	];

	public function employees()
	{
		return $this->hasMany(Employee::class, 'department_id');
	}
}
