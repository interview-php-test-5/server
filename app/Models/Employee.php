<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	const MONTHLY_RATE_TYPE = 'monthly_rate';
	const HOURLY_RATE_TYPE = 'hourly_rate';

	const JUNIOR_POSITION = 'junior';
	const MIDDLE_POSITION = 'middle';
	const SENIOR_POSITION = 'senior';

	protected $fillable = [
		'full_name',
		'birth_date',
		'type',
		'department_id',
		'position',
		'number_of_hours',
		'hourly_rate'
	];

	protected $appends = ['salary'];

	public function department()
	{
		return $this->belongsTo(Department::class);
	}

	public function getSalaryAttribute()
	{
		$salary = rand(1000, 2000);
		if($this->type == Employee::HOURLY_RATE_TYPE)
		{
			$salary = $this->number_of_hours * $this->hourly_rate;
		}
		return $salary;
	}
}
