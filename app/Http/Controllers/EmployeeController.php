<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$employeesQuery = Employee::select('employees.*', 'departments.name as department_name');

		if($request->department_id)
		{
			$employeesQuery->where('department_id', $request->department_id);
		}
		$employees = $employeesQuery
			->leftJoin('departments', 'departments.id', '=', 'employees.department_id')
			->orderBy('id', 'asc')
			->paginate($request->perPage);

		return $employees;
	}
}
