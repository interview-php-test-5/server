<?php

namespace App\Http\Controllers;

use App\Models\Department;

class DepartmentController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$departments = Department::all();
		return $departments;
	}
}
